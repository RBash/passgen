import json, nltk
from nltk.corpus import wordnet as wn
from collections import defaultdict

def dictGen():
    """
    Collecting (adjectives, nouns) from nltk and write them to dictionary.
    :return: True
    """
    # Comment out below if you've already downloaded NLTK dictionaries.
    nltk.download()

    pg_dict = defaultdict(list)


    # Collecting adjectives
    for adjectives in wn.all_synsets(pos=wn.ADJ):
        for lemma in adjectives.lemmas():
            if "-" not in lemma.name() and "_" not in lemma.name():
                pg_dict['adjectives'].append(lemma.name().title())

    # Collecting nouns
    for nouns in wn.all_synsets(pos=wn.NOUN):
        for lemma in nouns.lemmas():
            if "-" not in lemma.name() and "_" not in lemma.name() and "/" not in lemma.name():
                pg_dict['nouns'].append(lemma.name().title())

    # Writing to file
    with open('pg_dict.json', 'w') as dictfile:
        json.dump(pg_dict, dictfile)

    return True


if __name__ == '__main__':
    run = dictGen()

