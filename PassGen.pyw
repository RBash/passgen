# -*- coding: utf-8 -*-

# Yet another xkcd-style password generator.
#	xkcd.com/936/
# Created by: RBash


from PySide import QtCore, QtGui
import os, sys, random, json, ctypes


random.seed()
# Defining the count of passwords
global count
count = 6
# Defining the minimum password length
global passwMinLength
passwMinLength = 12


def start():
    ex = Ui_PassGen()
    ex.show()
    ex.generatePassLoop()
    return ex

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_PassGen(QtGui.QWidget):
    def __init__(self):
        super(Ui_PassGen, self).__init__()
        #QtGui.QWidget.__init__(self)
        self.setupUi(self)
        self.setWindowIcon(QtGui.QIcon('shakal_icon.png'))

        if os.name == 'nt':
            myappid = 'BRR.PassGen.0.2'  # arbitrary string
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)


    def setupUi(self, PassGen):
        PassGen.setObjectName(_fromUtf8("PassGen"))
        PassGen.resize(265, 390)
        PassGen.move(300, 300)
        PassGen.setStyleSheet("background-color: #676767; color: #ffffff")
        self.verticalLayout_2 = QtGui.QVBoxLayout(PassGen)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.textEdit = QtGui.QTextEdit(PassGen)
        self.textEdit.setMinimumSize(QtCore.QSize(0, 120))
        self.textEdit.setObjectName(_fromUtf8("textEdit"))
        self.textEdit.setFontPointSize(10)
        self.verticalLayout.addWidget(self.textEdit)
        self.refreshBtn = QtGui.QPushButton(PassGen)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.refreshBtn.sizePolicy().hasHeightForWidth())
        self.refreshBtn.setSizePolicy(sizePolicy)
        self.refreshBtn.setMinimumSize(QtCore.QSize(120, 60))
        self.refreshBtn.setBaseSize(QtCore.QSize(10, 5))
        self.refreshBtn.setStyleSheet("background-color: #676767; color: #bbbbbb")
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Arial Unicode"))
        font.setPointSize(18)
        font.setBold(False)
        font.setWeight(50)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.refreshBtn.setFont(font)
        self.refreshBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.refreshBtn.setObjectName(_fromUtf8("refreshBtn"))
        self.verticalLayout.addWidget(self.refreshBtn)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(PassGen)
        QtCore.QMetaObject.connectSlotsByName(PassGen)

    def retranslateUi(self, PassGen):
        PassGen.setWindowTitle(_translate("PassGen", "PassGen", None))
        self.refreshBtn.setText(_translate("PassGen", "REFRESH", None))
        self.refreshBtn.clicked.connect(self.generatePassLoop)


    def passwdGen(self, pg_dict):
        self.pswd = ""
        self.numPart = ""

        self.numPart = str(ord(os.urandom(1)))
        if len(self.numPart) == 3:
            self.pswd = self.numPart + random.choice(pg_dict['adjectives']) + random.choice(pg_dict['nouns'])

            return self.pswd
        else:
            return self.passwdGen(pg_dict)


    def generatePassLoop(self):
        self.textEdit.clear()
        passStrings = ""
        # Checking if dictionary exists
        if not os.path.exists('pg_dict.json'):
            self.textEdit.setText("The dictionary wasn't found. Please create one by DictGen")
            return False

        with open('pg_dict.json') as dictfile:
            pg_dict = json.load(dictfile)

        counter = 0
        while counter < count:
            counter += 1
            passStrings += str("{0}\n\n".format(self.passwdGen(pg_dict)))
        self.textEdit.setText("Here your passwords: \n\n\n{0}\nPress REFRESH for more.".format(passStrings))
        return True

if __name__ == '__main__':
    pg = QtGui.QApplication(sys.argv)
    window = start()
    pg.exec_()
	
